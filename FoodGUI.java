import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class FoodGUI {
    private JPanel root;
    private JButton torikaraButton;
    private JButton namaBeerButton;
    private JButton sashimiButton;
    private JButton potetoButton;
    private JButton edamameButton;
    private JButton yakitoriButton;
    private JButton checkOutButton;
    private JTextPane receivedInfo;
    private JTextPane numberOfItem;
    private JLabel totalPrice;

    int total_price = 0;
    int[] orderedItems = new int[6];

    void order(String food, int price) {
        int  confirmation1 = JOptionPane.showConfirmDialog(
                null,
                 food + "でよろしいですか?",
                "ご注文内容確認",
                JOptionPane.YES_NO_OPTION
        );
        if(confirmation1 == 0) {
            if(food == "生ビール") {
                int  confirmation2 = JOptionPane.showConfirmDialog(
                        null,
                        "20歳以上ですか?",
                        "年齢確認",
                        JOptionPane.YES_NO_OPTION
                );
                if (confirmation2 == 0) {
                    orderedItems[1] += 1;
                    String Received_Info = receivedInfo.getText();
                    receivedInfo.setText(Received_Info + food + "\n");
                    String Order_Items = numberOfItem.getText();
                    numberOfItem.setText(Order_Items + orderedItems[1] + "\n");
                    total_price += price;
                    totalPrice.setText(total_price + " 円");
                }
                else if (confirmation2 == 1) {
                    JOptionPane.showMessageDialog(null, "法律上、20歳未満の方にお酒を発売することはできません");
                }
                JOptionPane.showMessageDialog(null, "ご注文ありがとうございます!");
            }
            else {
                switch (food) {
                    case "鶏の唐揚げ":
                        orderedItems[0] += 1;
                        //String Received_Info = receivedInfo.getText();
                        numberOfItem.setText(orderedItems[0] + "\n");
                        break;
                    case "お造り盛り合わせ":
                        orderedItems[2] = 1;
                        numberOfItem.setText(orderedItems[1] + "\n");
                        break;
                    case "山盛りフライドポテト":
                        orderedItems[3] += 1;
                        numberOfItem.setText(orderedItems[3] + "\n");
                        break;
                    case "枝豆":
                        orderedItems[4] += 1;
                        numberOfItem.setText(orderedItems[4] + "\n");
                        break;
                    case "焼き鳥盛り合わせ":
                        orderedItems[5] += 1;
                        numberOfItem.setText(orderedItems[5] + "\n");
                        break;
                }
                JOptionPane.showMessageDialog(null, "ご注文ありがとうございます!");
                receivedInfo.setText(food + "\n");
                total_price += price;
                totalPrice.setText(total_price + " 円");
            }
        }
    }

    void cancel(int a){
        if (a == 0) {
            total_price = 0;
            for (int i = 0; i < 6; i++) {
                orderedItems[i] = 0;
            }
            totalPrice.setText("0" + " 円");
            numberOfItem.setText("0" + " 品");
            receivedInfo.setText("");
        }
    }

    public FoodGUI() {
        torikaraButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                order("鶏の唐揚げ", 380);
            }
        });
        torikaraButton.setIcon(new ImageIcon(this.getClass().getResource("鶏の唐揚げ.jpeg")));
        namaBeerButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("生ビール", 350);
            }
        });
        namaBeerButton.setIcon(new ImageIcon(this.getClass().getResource("生ビール.jpeg")));
        sashimiButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                order("お造り盛り合わせ", 980);
            }
        });
        sashimiButton.setIcon(new ImageIcon(this.getClass().getResource("お造りの盛り合わせ.jpeg")));
        potetoButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                order("山盛りフライドポテト", 480);
            }
        });
        potetoButton.setIcon(new ImageIcon(this.getClass().getResource("フライドポテト.jpeg")));
        edamameButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                order("枝豆", 280);
            }
        });
        edamameButton.setIcon(new ImageIcon(this.getClass().getResource("枝豆.jpeg")));
        yakitoriButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                order("焼き鳥盛り合わせ", 840);
            }
        });
        yakitoriButton.setIcon(new ImageIcon(this.getClass().getResource("焼き鳥.jpeg")));

        checkOutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation1 = JOptionPane.showConfirmDialog(null, "以上でよろしいですか?\n注文内容:\n鶏の唐揚げ " + orderedItems[0] + "品\n生ビール " + orderedItems[1] +
                                "品\nお造り盛り合わせ " + orderedItems[2] + "品\n山盛りフライドポテト " + orderedItems[3] + "品\n枝豆 " + orderedItems[4] + "品\n焼き鳥盛り合わせ " + orderedItems[5] + "品\n",
                        "注文内容確認",
                        JOptionPane.YES_NO_OPTION);
                if (confirmation1 == 0 && total_price == 0) {
                    JOptionPane.showMessageDialog(null, "商品が注文されていません\n");
                }
                if (confirmation1 == 0 && total_price != 0) {
                    JOptionPane.showMessageDialog(null, "合計で " + total_price + " 円になります\nありがとうございました!");
                    cancel(confirmation1);
                }
                if (confirmation1 == 1) {
                    int confirmation2 = JOptionPane.showConfirmDialog(null, "注文を取り消しますか?\nそうであれば はい を押して下さい\n",
                            "ご注文取消",
                            JOptionPane.YES_NO_OPTION);
                    if (confirmation2 == 0) {
                        cancel(confirmation2);
                    }
                }
            }
        });

    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("FoodGUI");
        frame.setContentPane(new FoodGUI().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
